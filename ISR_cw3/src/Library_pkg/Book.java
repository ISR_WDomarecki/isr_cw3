package Library_pkg;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class Book implements Serializable {
    public String title;
    public String author;
    public String book_number;
    public String owner = "";
    public LinkedList<String> querry = new LinkedList<>();

    Book(String title, String author, String book_number){
        this.title = title;
        this.author = author;
        this.book_number = book_number;
    }
}
