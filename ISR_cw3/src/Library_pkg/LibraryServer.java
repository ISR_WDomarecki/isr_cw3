package Library_pkg;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class LibraryServer {
    Registry reg;

    public static void main(String[] args) {
        try {
            new LibraryServer();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    protected LibraryServer() {
        try{
            reg = LocateRegistry.createRegistry(1099);
            LibraryServerImpl servant = new LibraryServerImpl();
            reg.rebind("LibraryServer", servant);
            System.out.println("LibraryServer ready");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
    }
}
