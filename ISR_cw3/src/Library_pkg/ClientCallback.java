package Library_pkg;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ClientCallback extends UnicastRemoteObject implements ICallback {
    public ClientCallback() throws RemoteException {
    }

    public void inform(String message) {
        System.out.println(message);
    }
}
