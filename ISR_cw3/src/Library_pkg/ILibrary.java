package Library_pkg;

import java.rmi.*;
import java.util.List;

public interface ILibrary extends Remote {
    List<Book> getBookList() throws RemoteException;
    ReturnState reserveBook(String title, String author, String isbn, String userName, ICallback callback) throws RemoteException;
    boolean returnBook(String title, String author, String isbn, String userName) throws RemoteException;

}