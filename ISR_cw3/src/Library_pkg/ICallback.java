package Library_pkg;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICallback extends Remote {
    void inform(String message) throws RemoteException;
}
