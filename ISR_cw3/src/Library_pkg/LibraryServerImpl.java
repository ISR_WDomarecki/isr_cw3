package Library_pkg;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LibraryServerImpl extends UnicastRemoteObject implements ILibrary {

    LibraryServerImpl() throws RemoteException {
        books.add(new Book("a", "b", "c"));
        books.add(new Book("Jakis tytul", "Jakis autor", "Jakis book_number"));
        books.add(new Book("t1", "a1", "is1"));
        books.add(new Book("t2", "a2", "is2"));
    }

    List<Book> books = new LinkedList<>();
    Map<String, ICallback> users = new HashMap<>();

    @Override
    public List<Book> getBookList() {
        return books;
    }

    @Override
    public ReturnState reserveBook(String title, String author, String book_number, String userName, ICallback callback) {
        Book foundBook = findBook(title, author, book_number);
        users.put(userName,callback);
        if (!(foundBook == null)) {
            if (foundBook.owner.isEmpty()) {
                foundBook.owner = userName;
                System.out.println(String.join(" ","reserved book:", foundBook.title, foundBook.author, foundBook.book_number, "for", userName));
                return ReturnState.reserved;
            } else {
                foundBook.querry.add(userName);
                System.out.println(String.join("gueued", title, author, book_number, "for", userName));
                return ReturnState.queued;
            }
        } else {
            System.out.println("invalid");
            return ReturnState.invalid;
        }
    }

    @Override
    public boolean returnBook(String title, String author, String book_number, String userName) {
        Book foundBook = findBook(title, author, book_number);
        Boolean result = false;
        if (!(foundBook == null)) {
            if (foundBook.owner.equals(userName)) {
                foundBook.owner = "";
                System.out.println(String.join(" ","Returned book", foundBook.title, foundBook.author, foundBook.book_number,"by",userName));
                result = true;
                if (!foundBook.querry.isEmpty())
                {
                    String user = foundBook.querry.removeFirst();
                    foundBook.owner = user;
                    System.out.println(String.join(" ","Informing ",user));
                    ICallback callback = users.get(user);
                    try {
                        if (callback != null)
                            callback.inform(String.join(" ", "Book ready:", title, author, book_number));
                        else
                            System.out.println("User not found");
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
        return result;
    }

    private Book findBook(String title, String author, String book_number) {
        Book foundBook = null;
        for (Book book : books) {
            if (book.title.equals(title) && book.author.equals(author) && book.book_number.equals(book_number)) {
                foundBook = book;
            }
        }
        return foundBook;
    }
}
