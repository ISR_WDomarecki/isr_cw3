package Library_pkg;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.List;

public class Client{
    private String host;
    private Scanner userInput = new Scanner(System.in);
    private String username;
    private ClientCallback callback;

    Client(String host) {
        this.host = host;
        System.out.println("Choose name:");
        if (userInput.hasNextLine())
            username = userInput.nextLine();
        Registry reg;
        try {
            callback = new ClientCallback();
            reg = LocateRegistry.getRegistry(host);
            ILibrary library = (ILibrary) reg.lookup("LibraryServer");
            List<Book> books = library.getBookList();
            System.out.println("Book list:");
            for (Book book : books) {
                System.out.println(String.join(" ", book.title, book.author, book.book_number));
            }
            while (true) {
                System.out.println("1 - reserve book, 2 - return book");
                String ans = "", title = "", author = "", book_number = "";
                while (!ans.equals("1") && !ans.equals("2") && userInput.hasNextLine())
                    ans = userInput.nextLine();
                System.out.println("Choose title:");
                if (userInput.hasNextLine())
                    title = userInput.nextLine();
                System.out.println("Choose author:");
                if (userInput.hasNextLine())
                    author = userInput.nextLine();
                System.out.println("Choose book_number:");
                if (userInput.hasNextLine())
                    book_number = userInput.nextLine();
                if (ans.equals("1")) {
                    ReturnState state = library.reserveBook(title, author, book_number, username, callback);
                    if (state == ReturnState.reserved) {
                        System.out.println("Reserved");
                    } else if (state == ReturnState.queued) {
                        System.out.println("Queued");
                    } else {
                        System.out.println("Invalid querry");
                    }
                } else if (ans.equals("2")) {
                    Boolean state = library.returnBook(title, author, book_number, username);
                    if (state)
                        System.out.println("Returned");
                    else
                        System.out.println("Failed");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.printf("Usage: ChatClient  <server  host host >");
            System.exit(-1);
        }
        new Client(args[0]);
    }
}
